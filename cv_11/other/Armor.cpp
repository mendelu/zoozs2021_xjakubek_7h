//
// Created by xjakubek on 25.11.2021.
//

#include "Armor.h"

Armor::Armor(std::string name, int defence) {
    m_name = name;
    m_defence = defence;
}

std::string Armor::getName() {
    return m_name;
}

int Armor::getDefence() {
    return m_defence;
}