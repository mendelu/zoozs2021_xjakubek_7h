//
// Created by xjakubek on 25.11.2021.
//

#include "Hero.h"

Hero::Hero() {
    m_health = 0;
    m_armor = nullptr;
    m_strength = 0;
    m_weapon = nullptr;
    m_items = {};
}

void Hero::setArmor(Armor *a) {
    m_armor = a;
}

void Hero::setWeapon(Weapon *w) {
    m_weapon = w;
}

void Hero::setHealth(int h) {
    m_health = h;
}

void Hero::setStrength(int s) {
    m_strength = s;
}

void Hero::addItem(Item *i) {
    if (m_items.size() < 5) {
        m_items.push_back(i);
    } else {
        std::cout << "Viac uz neunesies!" << std::endl;
    }
}

void Hero::removeItem(int index) {
    if (index >= 0 && index < m_items.size()) {
        m_items.erase(m_items.begin() + index);
        std::cout << "Predmet bol vyhodeny" << std::endl;
    } else {
        std::cout << "Zla pozicia v invetari!" << std::endl;
    }
}

void Hero::printInfo() {
    std::cout << "Health: " << m_health << std::endl;
    std::cout << "Strength: " << m_strength << std::endl;

    if (m_weapon != nullptr) {
        std::cout << "Weapon: " << m_weapon->getName() << std::endl;
    } else {
        std::cout << "No weapon!" << std::endl;
    }

    if (m_armor != nullptr) {
        std::cout << "Armor: " << m_armor->getName() << std::endl;
    } else {
        std::cout << "No armor!" << std::endl;
    }

    for (auto *item:m_items) {
        std::cout << "Item: " << item->getName() << std::endl;
    }
}