//
// Created by xjakubek on 25.11.2021.
//

#ifndef CV_11_HERO_H
#define CV_11_HERO_H

#include <vector>

#include "Weapon.h"
#include "Armor.h"
#include "Item.h"

class Hero {
    int m_health;
    int m_strength;
    Weapon *m_weapon;
    Armor *m_armor;
    std::vector<Item *> m_items;
public:
    Hero();

    void setWeapon(Weapon *w);

    void setArmor(Armor *a);

    void addItem(Item *i);

    void removeItem(int index);

    void printInfo();

    void setHealth(int h);

    void setStrength(int s);

    int getAttackDamage();

    int getDefence();
};


#endif //CV_11_HERO_H
