//
// Created by xjakubek on 25.11.2021.
//

#ifndef CV_11_ITEM_H
#define CV_11_ITEM_H

#include <iostream>

class Item {
    std::string m_name;
    std::string m_bonusType;
    int m_bonus;
public:
    Item(std::string name, std::string type, int bonus);

    std::string getName();

    std::string getBonusType();

    int getBonus();
};


#endif //CV_11_ITEM_H
