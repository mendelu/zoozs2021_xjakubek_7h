//
// Created by xjakubek on 25.11.2021.
//

#ifndef CV_11_WEAPON_H
#define CV_11_WEAPON_H

#include <iostream>

class Weapon {
    std::string m_name;
    int m_damage;

public:
    Weapon(std::string name, int damage);

    std::string getName();

    int getDamage();
};


#endif //CV_11_WEAPON_H
