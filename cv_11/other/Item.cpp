//
// Created by xjakubek on 25.11.2021.
//

#include "Item.h"

Item::Item(std::string name, std::string type, int bonus) {
    m_name = name;
    m_bonusType = type;
    m_bonus = bonus;
}

std::string Item::getName() {
    return m_name;
}

int Item::getBonus() {
    return m_bonus;
}

std::string Item::getBonusType() {
    return m_bonusType;
}