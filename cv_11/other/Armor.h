//
// Created by xjakubek on 25.11.2021.
//

#ifndef CV_11_ARMOR_H
#define CV_11_ARMOR_H

#include <iostream>

class Armor {
    std::string m_name;
    int m_defence;
public:
    Armor(std::string name, int defence);

    std::string getName();

    int getDefence();
};


#endif //CV_11_ARMOR_H
