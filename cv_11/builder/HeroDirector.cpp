//
// Created by xjakubek on 25.11.2021.
//

#include "HeroDirector.h"

HeroDirector::HeroDirector(HeroBuilder *builder) {
    m_builder = builder;
}

void HeroDirector::setBuilder(HeroBuilder *builder) {
    m_builder = builder;
}

Hero * HeroDirector::constructHero() {
    m_builder->createHero();
    m_builder->setAttributes();
    m_builder->setItems();
    m_builder->setArmor();
    m_builder->setWeapon();
    return m_builder->getHero();
}