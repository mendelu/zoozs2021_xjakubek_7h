//
// Created by xjakubek on 25.11.2021.
//

#ifndef CV_11_HEROBUILDER_H
#define CV_11_HEROBUILDER_H

#include "../other/Hero.h"

class HeroBuilder {
protected:
    Hero *m_hero;
public:
    HeroBuilder();

    void createHero();

    Hero *getHero();

    virtual void setAttributes() = 0;

    virtual void setWeapon() = 0;

    virtual void setArmor() = 0;

    virtual void setItems() = 0;
};


#endif //CV_11_HEROBUILDER_H
