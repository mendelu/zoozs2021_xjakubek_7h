//
// Created by xjakubek on 25.11.2021.
//

#include "HeroBuilder.h"

#ifndef CV_11_ROGUEHEROBUILDER_H
#define CV_11_ROGUEHEROBUILDER_H


class RogueHeroBuilder : public HeroBuilder {
public:
    void setAttributes() override;

    void setWeapon() override;

    void setArmor() override;

    void setItems() override;
};


#endif //CV_11_ROGUEHEROBUILDER_H
