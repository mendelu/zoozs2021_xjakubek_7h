//
// Created by xjakubek on 25.11.2021.
//

#ifndef CV_11_BARBARIANHEROBUILDER_H
#define CV_11_BARBARIANHEROBUILDER_H

#include "HeroBuilder.h"

class BarbarianHeroBuilder : public HeroBuilder {
public:
    void setAttributes() override;

    void setWeapon() override;

    void setArmor() override;

    void setItems() override;
};


#endif //CV_11_BARBARIANHEROBUILDER_H
