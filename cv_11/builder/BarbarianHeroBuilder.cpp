//
// Created by xjakubek on 25.11.2021.
//

#include "BarbarianHeroBuilder.h"

void BarbarianHeroBuilder::setAttributes() {
    m_hero->setStrength(250);
    m_hero->setHealth(100);
}

void BarbarianHeroBuilder::setWeapon() {
    m_hero->setWeapon(new Weapon("Barbarian weapon", 10));
}

void BarbarianHeroBuilder::setArmor() {
    m_hero->setArmor(new Armor("Barbarian basic armor", 20));
}

void BarbarianHeroBuilder::setItems() {
    m_hero->addItem(new Item("Health Potion", "health", 20));
    m_hero->addItem(new Item("Strength Potion", "strength", 10));
}