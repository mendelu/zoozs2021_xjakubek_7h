//
// Created by xjakubek on 25.11.2021.
//

#include "RogueHeroBuilder.h"

void RogueHeroBuilder::setWeapon() {
    m_hero->setWeapon(new Weapon("Rogue daggers", 50));
}

void RogueHeroBuilder::setAttributes() {
    m_hero->setHealth(30);
    m_hero->setStrength(5);
}

void RogueHeroBuilder::setArmor() {
//    m_hero->setArmor()
}

void RogueHeroBuilder::setItems() {
    m_hero->addItem(new Item("Invisibility Potion", "health", 50));
}