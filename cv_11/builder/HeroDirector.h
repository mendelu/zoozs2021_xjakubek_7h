//
// Created by xjakubek on 25.11.2021.
//

#ifndef CV_11_HERODIRECTOR_H
#define CV_11_HERODIRECTOR_H

#include "HeroBuilder.h"

class HeroDirector {
    HeroBuilder *m_builder;
public:
    HeroDirector(HeroBuilder *builder);

    void setBuilder(HeroBuilder *builder);

    Hero *constructHero();
};


#endif //CV_11_HERODIRECTOR_H
