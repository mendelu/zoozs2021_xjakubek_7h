#include <iostream>
#include "builder/HeroDirector.h"
#include "builder/BarbarianHeroBuilder.h"
#include "builder/RogueHeroBuilder.h"

int main() {
    HeroDirector *director = new HeroDirector(new BarbarianHeroBuilder());

    Hero *hero = director->constructHero();

    hero->printInfo();

    std::cout << std::endl;

    director->setBuilder(new RogueHeroBuilder());
    hero = director->constructHero();
    hero->printInfo();

    delete hero;
    delete director;
    return 0;
}
