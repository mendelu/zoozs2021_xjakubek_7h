#include "System.h"

int main() {
    System::createPatient("Tomas");
    System::createPatient("Martin");

    auto *dg1 = new Diagnosis("kaz pokrocily");
    auto *dg2 = new Diagnosis("parodontitis");
    auto *dg3 = new Diagnosis("faseta");

    System::enterNewDiagnoses({dg1, dg2}, 0, 0, 14);
    System::enterNewDiagnoses({dg3}, 1, 1, 5);
    System::printPatientsInfo();

    return 0;
}
