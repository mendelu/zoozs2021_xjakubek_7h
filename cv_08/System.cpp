//
// Created by xjakubek on 04.11.2021.
//

#include "System.h"

int System::s_patientsCount = 0;
std::vector<Patient *> System::s_patients = {};

void System::createPatient(std::string name) {
    Patient *newPatient = new Patient(name, s_patientsCount);
    s_patientsCount++;
    s_patients.push_back(newPatient);
}

Patient *System::findPatient(int id) {
    Patient *foundPatient = nullptr;

    for (Patient *patient: s_patients) {
        if (patient->getId() == id) {
            foundPatient = patient;
            break;
        }
    }

    if (foundPatient == nullptr) {
        std::cout << "Patient with ID " << id << " was not found in system!" << std::endl;

    }

    return foundPatient;
}

void System::enterNewDiagnoses(std::vector<Diagnosis *> diagnoses, int patientId, int jawIndex, int toothIndex) {
    Patient *foundPatient = findPatient(patientId);
    if (foundPatient == nullptr) {
        return;
    }

    auto teeth = foundPatient->getTeeth();

    // optional
    if (teeth[jawIndex][toothIndex] == nullptr) {
        std::cout << "Tooth doesn't exist!" << std::endl;
        return;
    }

    teeth[jawIndex][toothIndex]->addDiagnoses(diagnoses);
    std::cout << "Diagnoses were saved!" << std::endl;
}

void System::printPatientsInfo() {
    for (auto patient: s_patients) {
        patient->printInfo();
        std::cout << "----" << std::endl;
    }
}