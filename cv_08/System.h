//
// Created by xjakubek on 04.11.2021.
//

#ifndef TEETH_SYSTEM_H
#define TEETH_SYSTEM_H

#include "Patient.h"
#include "Diagnosis.h"

class System {
    static std::vector<Patient *> s_patients;
    static int s_patientsCount;

public:
    static void createPatient(std::string name);

    static Patient *findPatient(int id);

    static void enterNewDiagnoses(std::vector<Diagnosis *> diagnoses, int patientId, int jawIndex, int toothIndex);

    static void printPatientsInfo();
};


#endif //TEETH_SYSTEM_H
