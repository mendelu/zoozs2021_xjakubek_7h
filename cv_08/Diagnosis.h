//
// Created by xjakubek on 04.11.2021.
//

#ifndef TEETH_DIAGNOSIS_H
#define TEETH_DIAGNOSIS_H


#include <string>

class Diagnosis {
    std::string m_description;

public:
    Diagnosis(std::string description);
};


#endif //TEETH_DIAGNOSIS_H
