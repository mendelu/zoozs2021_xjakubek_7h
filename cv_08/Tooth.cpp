//
// Created by xjakubek on 04.11.2021.
//

#include "Tooth.h"

Tooth::Tooth(std::string type) {
    m_type = type;
    m_diagnoses = {};
}

std::vector<Diagnosis *> Tooth::getDiagnoses() {
    return m_diagnoses;
}

void Tooth::addDiagnoses(std::vector<Diagnosis *> dgs) {
    if (m_diagnoses.size() > 0) {
        std::cout << "Tooth has diagnoses already!" << std::endl;
    }
    m_diagnoses.insert(m_diagnoses.end(), dgs.begin(), dgs.end());
}