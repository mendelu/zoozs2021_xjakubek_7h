//
// Created by xjakubek on 04.11.2021.
//

#include "Patient.h"

Patient::Patient(std::string name, int id) {
    m_name = name;
    m_id = id;
    generateTeeth();
}

std::array<std::array<Tooth *, 16>, 2> Patient::getTeeth() {
    return m_teeth;
}

int Patient::getId() {
    return m_id;
}

void Patient::printInfo() {
    std::cout << "Name: " << m_name << std::endl;
    std::cout << "Id: " << m_id << std::endl;
    std::cout << "Upper jaw diagnoses: " << calcDgsCountOnJaw(0) << std::endl;
    std::cout << "Lower jaw diagnoses: " << calcDgsCountOnJaw(1) << std::endl;
}

void Patient::generateTeeth() {
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 16; j++) {
            std::string type = "";
            if (j < 3 or j > 12) {
                type = "Molar";
            } else if (j < 5 or j > 10) {
                type = "Premolar";
            } else if (j == 5 or j == 10) {
                type = "Spicak";
            } else {
                type = "Rezak";
            }
            m_teeth.at(i).at(j) = new Tooth(type);
        }
    }
}

int Patient::calcDgsCountOnJaw(int index) {
    int count = 0;
    for (auto tooth: m_teeth.at(index)) {
        count += tooth->getDiagnoses().size();
    }
    return count;
}

