//
// Created by xjakubek on 04.11.2021.
//

#ifndef TEETH_PATIENT_H
#define TEETH_PATIENT_H

#include <array>
#include "Tooth.h"

class Patient {
    std::string m_name;
    int m_id;
    std::array<std::array<Tooth *, 16>, 2> m_teeth;

public:
    Patient(std::string name, int id);

    std::array<std::array<Tooth *, 16>, 2> getTeeth();

    int getId();

    void printInfo();

private:
    void generateTeeth();

    int calcDgsCountOnJaw(int index);
};


#endif //TEETH_PATIENT_H
