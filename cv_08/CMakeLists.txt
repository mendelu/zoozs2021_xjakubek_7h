cmake_minimum_required(VERSION 3.19)
project(cv_08)

set(CMAKE_CXX_STANDARD 14)

add_executable(cv_08 main.cpp Diagnosis.cpp Diagnosis.h Tooth.cpp Tooth.h Patient.cpp Patient.h System.cpp System.h)
