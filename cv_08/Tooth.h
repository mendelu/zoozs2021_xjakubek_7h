//
// Created by xjakubek on 04.11.2021.
//

#ifndef TEETH_TOOTH_H
#define TEETH_TOOTH_H

#include <iostream>
#include "Diagnosis.h"
#include <vector>

class Tooth {
    std::string m_type;
    std::vector<Diagnosis *> m_diagnoses;

public:
    Tooth(std::string type);

    std::vector<Diagnosis *> getDiagnoses();

    void addDiagnoses(std::vector<Diagnosis *> dgs);
};


#endif //TEETH_TOOTH_H
