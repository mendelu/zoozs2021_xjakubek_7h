#include <iostream>

using namespace std;

class Drak {
private:
    float m_zdravie;
    float m_obrana;
    float m_sila;
public:
    Drak(float obrana, float sila) {
        m_obrana = obrana;
        m_sila = sila;
        m_zdravie = 100;
    }

    float getZdravie() {
        return m_zdravie;
    }

    float getSila() {
        return m_sila;
    }

    float getObrana() {
        return m_obrana;
    }

    void uberZdravie(float kolkoZdravia) {
        m_zdravie = m_zdravie - kolkoZdravia;
    }
};

class Rytier {
private:
    float m_zdravie;
    float m_obrana;
    float m_sila;
    string m_meno;
public:
    Rytier(float obrana, float sila, string meno) {
        setObrana(obrana);
        m_sila = sila;
        m_zdravie = 100;
        m_meno = meno;
    }

    float getZdravie() {
        return m_zdravie;
    }

    float getSila() {
        return m_sila;
    }

    float getObrana() {
        return m_obrana;
    }

    string getMeno() {
        return m_meno;
    }

    void fight(Drak *nepriatel) {
        float silaNepriatela = nepriatel->getSila();
        float obranaNepriatela = nepriatel->getObrana();

        if (m_sila > obranaNepriatela) {
            // rytier je silnejsi nez drak
            nepriatel->uberZdravie(m_sila - obranaNepriatela);
        }

        if (silaNepriatela > m_obrana) {
            // drak je silnejsi nez rytier
            m_zdravie -= silaNepriatela - m_obrana;
        }
    }

private:
    // kontroluje ci je to v <0, 200>
    void setObrana(float obrana) {
        if ((obrana >= 0) and (obrana <= 200)) {
            m_obrana = obrana;
        } else {
            // zalogovat chybu do logeru
            m_obrana = 0;
        }
    }
};


int main() {
    Drak *smak = new Drak(180, 360);
    Rytier *artus = new Rytier(70, 70, "Artuš");

    artus->fight(smak);
    cout << smak->getZdravie() << endl;
    cout << artus->getZdravie() << endl;

    delete smak;
    delete artus;
    return 0;
}
