#include <iostream>

using namespace std;

class Balik {
public:
    float m_vaha;
    string m_prijemca;
    string m_odosielatel;
    int m_dobierka;

    Balik(string prijemca, string odosielatel, float vaha) {
        m_prijemca = prijemca;
        m_odosielatel = odosielatel;
        m_vaha = vaha;
        m_dobierka = 0;
    }

    Balik(string prijemca, string odosielatel, float vaha,
          int dobierka) {
        m_prijemca = prijemca;
        m_odosielatel = odosielatel;
        m_dobierka = dobierka;
        m_vaha = vaha;
    }

    Balik(string prijemca, string odosielatel) : Balik(prijemca, odosielatel, 0.0, 0) {}

    Balik(string prijemca, string odosielatel, int dobierka) {
        m_prijemca = prijemca;
        m_odosielatel = odosielatel;
        m_vaha = 0.0;
        m_dobierka = dobierka;
    }

    void setVaha(float novaVaha) {
        if (novaVaha < 0) {
            m_vaha = 0;
            cout << "Zadana nevalidna vaha!!!!" << endl;
        } else {
            m_vaha = novaVaha;
        }
    }

    void printInfo() {
        cout << "Prijemca: " << m_prijemca << endl;
        cout << "Odosielatel: " << m_odosielatel << endl;
        cout << "Vaha: " << m_vaha << endl;
        cout << "Dobierka: " << m_dobierka << endl;
    }


};

int main() {
    Balik *balik1 = new Balik("Filip", "Tomas", 1000);
    balik1->printInfo();
    balik1->setVaha(5.5);
    balik1->printInfo();

    return 0;
}
