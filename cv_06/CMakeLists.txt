cmake_minimum_required(VERSION 3.19)
project(cv_06)

set(CMAKE_CXX_STANDARD 14)

add_executable(cv_06 main.cpp)