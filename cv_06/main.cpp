#include <iostream>
#include <array>
#include <vector>

using namespace std;

class Box {
    float m_weight;
    string m_content;
    string m_owner;

public:
    Box(float w, string c, string o) {
        m_weight = w;
        m_content = c;
        m_owner = o;
    }

    string getContent() {
        return m_content;
    }

    string getOwner() {
        return m_owner;
    }
};

class Floor {
    string m_label;
    array<Box *, 10> m_positions;

public:
    Floor(string label) {
        m_label = label;
        for (int i = 0; i < m_positions.size(); ++i) {
            m_positions.at(i) = nullptr;
        }
    }

    void storeBox(Box *box, int position) {
        if ((position >= 0) and (position < m_positions.size())) {
            // kontrola obsadenosti
            if (m_positions.at(position) == nullptr) {
                // pozicia je prazdna -> mozem ulozit box
                m_positions.at(position) = box;
            } else {
                cout << "There is box at position " << position << "\n";
            }
        } else {
            cout << "You are saving out of range!" << endl;
        }
    }

    void removeBox(int position) {
        if ((position >= 0) and (position < m_positions.size())) {
            // kontrola obsadenosti
            if (m_positions.at(position) != nullptr) {
                // pozicia nie je prazdna -> mozem box odstranit
                m_positions.at(position) = nullptr;
            } else {
                cout << "There is no box at position " << position << "\n";
            }
        } else {
            cout << "You are saving out of range!" << endl;
        }
    }

    void printInfo() {
        cout << endl << "Floor status: " << endl;
        for (auto *currentBox: m_positions) {
            if (currentBox != nullptr) {
                cout << currentBox->getContent() << endl;
            } else {
                cout << "Position is empty." << endl;
            }
        }
    }

};

class Store {
    vector<Floor *> m_floors;
public:
    Store() {
        m_floors.push_back(new Floor("Floor n.0"));
    }

    void buildNewFloor() {
        m_floors.push_back(new Floor("Floor n." + to_string(m_floors.size())));
    }

    void storeBox(int floor, int position, Box *box) {
        // TODO: kontrola ci poschodie existuje
        m_floors.at(floor)->storeBox(box, position);
    }

    void printInfo() {
        for (auto *floor:m_floors) {
            floor->printInfo();
        }
    }

    ~Store() {
        for (auto *floor:m_floors) {
            delete (floor);
        }
    }
};


int main() {
    auto *boxik = new Box(100, "lentilky", "Tomas");
    Store *store = new Store();
    store->buildNewFloor();
    store->buildNewFloor();
    store->buildNewFloor();
    store->storeBox(1, 4, boxik);
    store->printInfo();

//    auto *floor1 = new Floor("n.1");
//    floor1->storeBox(boxik, 5);
//    floor1->removeBox(5);
//    floor1->printInfo();

    delete boxik;
//    delete floor1;
    delete store;
    return 0;
}
