//
// Created by xjakubek on 11.11.2021.
//

#ifndef ADVENTURA_GAMEENGINE_H
#define ADVENTURA_GAMEENGINE_H

#include "ObjektovaHraciDeska.h"
#include "Planeta.h"

class GameEngine {
private:
    ObjektovaHraciDeska *m_hraciDeska;
public:
    GameEngine();

    void hraj();

    ~GameEngine();

private:
    void inicializujBunky();

    void vypisNapovedu();
};


#endif //ADVENTURA_GAMEENGINE_H
