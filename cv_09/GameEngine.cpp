//
// Created by xjakubek on 11.11.2021.
//

#include "GameEngine.h"


GameEngine::GameEngine() {
    m_hraciDeska = new ObjektovaHraciDeska(3, 3);
    inicializujBunky();
}

void GameEngine::inicializujBunky() {
    // prvy riadok
    m_hraciDeska->vloz(0, 0, new Planeta("saturn", 40));
    m_hraciDeska->vloz(0, 1, new Planeta("uran", 10));
    m_hraciDeska->vloz(0, 2, new Planeta("neptun", 80));
    // druhy riadok
    m_hraciDeska->vloz(1, 0, new Planeta("pluto", 1));
    m_hraciDeska->vloz(1, 1, new Planeta("vega", 40));
    m_hraciDeska->vloz(1, 2, new Planeta("io", 15));
    // treti riadok
    m_hraciDeska->vloz(2, 0, new Planeta("mars", 80));
    m_hraciDeska->vloz(2, 1, new Planeta("zem", 20));
    m_hraciDeska->vloz(2, 2, new Planeta("jupiter", 22));
}

void GameEngine::hraj() {
    int rozhodnuti = 0;

    do {
        std::cout << "Vyber, co chces udelat ([0] exit, [1] napoveda): ";
        std::cin >> rozhodnuti;

        switch (rozhodnuti) {
            case 1:
                vypisNapovedu();
                break;
            case 2:
                m_hraciDeska->vypisDesku();
                break;
            default:
                break;
        }

    } while (rozhodnuti != 0);
}

void GameEngine::vypisNapovedu() {
    std::cout << "-------------------" << std::endl;
    std::cout << "[0]: konec hry" << std::endl;
    std::cout << "[1]: napoveda" << std::endl;
    std::cout << "[2]: vypsani hraci desky" << std::endl;
    std::cout << "-------------------" << std::endl;
}

GameEngine::~GameEngine() {
    m_hraciDeska->vymazDesku();
    delete m_hraciDeska;
}