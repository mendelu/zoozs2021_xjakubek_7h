//
// Created by xjakubek on 11.11.2021.
//

#ifndef ADVENTURA_PLANETA_H
#define ADVENTURA_PLANETA_H

#include "HraciPole.h"

class Planeta : public HraciPole {
private:
    int m_ulozeneMnozstviRudy;
public:
    Planeta(std::string popis, int dostupneMnozstviRudy);

    int vytazDavkuRudy();
};


#endif //ADVENTURA_PLANETA_H
