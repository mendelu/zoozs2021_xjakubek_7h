//
// Created by xjakubek on 11.11.2021.
//

#include "Planeta.h"

Planeta::Planeta(std::string popis, int dostupneMnozstviRudy) : HraciPole(popis) {
    m_ulozeneMnozstviRudy = dostupneMnozstviRudy;
}

int Planeta::vytazDavkuRudy() {
    int davka = m_ulozeneMnozstviRudy / 20;
    m_ulozeneMnozstviRudy -= davka;
    return davka;
}