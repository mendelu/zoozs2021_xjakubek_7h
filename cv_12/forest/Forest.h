//
// Created by xjakubek on 02.12.2021.
//

#ifndef CV_12_FOREST_H
#define CV_12_FOREST_H

#include "../Item.h"
#include "../NPC.h"
#include "ForestState.h"
#include "EnumTime.h"
#include "ForestStatePast.h"
#include "ForestStateFuture.h"
#include "ForestStatePresent.h"

class Forest {
    ForestState *m_state;
    std::string m_description;
    std::vector<Item *> m_items;
    std::vector<std::string> m_options;
    std::vector<NPC *> m_npcs;
    EnumTime m_time;

public:
    Forest();

    void printInfo();

    void setDescription(std::string desc);

    void setOptions(std::vector<std::string> o);

    void setNPCs(std::vector<NPC *> n);

    void setItems(std::vector<Item *> i);

    void changeState(EnumTime time);

private:
    void makeChanges();
};


#endif //CV_12_FOREST_H
