//
// Created by xjakubek on 02.12.2021.
//

#ifndef CV_12_ENUMTIME_H
#define CV_12_ENUMTIME_H

enum class EnumTime {
    Past, Present, Future
};

#endif //CV_12_ENUMTIME_H
