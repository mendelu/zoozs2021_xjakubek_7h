//
// Created by xjakubek on 02.12.2021.
//

#ifndef CV_12_FORESTSTATEPAST_H
#define CV_12_FORESTSTATEPAST_H

#include "ForestState.h"

class ForestStatePast : public ForestState {
    std::string getDescription() override;

    std::vector<std::string> getOptions() override;

    std::vector<Item *> getItems() override;

    std::vector<NPC *> getNPCs() override;
};


#endif //CV_12_FORESTSTATEPAST_H
