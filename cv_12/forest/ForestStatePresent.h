//
// Created by xjakubek on 02.12.2021.
//

#ifndef CV_12_FORESTSTATEPRESENT_H
#define CV_12_FORESTSTATEPRESENT_H

#include "ForestState.h"

class ForestStatePresent : public ForestState {
    std::string getDescription() override;

    std::vector<std::string> getOptions() override;

    std::vector<Item *> getItems() override;

    std::vector<NPC *> getNPCs() override;
};


#endif //CV_12_FORESTSTATEPRESENT_H
