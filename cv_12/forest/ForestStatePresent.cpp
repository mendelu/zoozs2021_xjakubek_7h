//
// Created by xjakubek on 02.12.2021.
//

#include "ForestStatePresent.h"

std::string ForestStatePresent::getDescription() {
    return "Nachazis se v lese v pritomnosti";
}

std::vector<Item *> ForestStatePresent::getItems() {
    return std::vector<Item *>{new Item("Rozbita pracka", "Pracka s rozbitym sklickem"),
                               new Item("Siska", "Z borovice")};

}

std::vector<std::string> ForestStatePresent::getOptions() {
    return std::vector<std::string>{"Prozkoumej les", "Pozdrav lesnika", "Seber sisku", "Mrkni na pracku"};
}

std::vector<NPC *> ForestStatePresent::getNPCs() {
    return std::vector<NPC *>{new NPC("Lesnik", "Dobrej den")};

}