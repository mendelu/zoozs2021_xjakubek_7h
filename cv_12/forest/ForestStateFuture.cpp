//
// Created by xjakubek on 02.12.2021.
//

#include "ForestStateFuture.h"

std::string ForestStateFuture::getDescription() {
    return "Nachazis se v lese v budoucnosti";
}

std::vector<Item *> ForestStateFuture::getItems() {
    return std::vector<Item *>{new Item("Rozbita pracka", "Tato pracka je zarostla mechem"),
                               new Item("Vesmirnou lod", "Havarovana vesmirna lod")};
}

std::vector<NPC *> ForestStateFuture::getNPCs() {
    return std::vector<NPC *>{new NPC("Mimozemstan", "))(&$%#($)$#(^")};
}

std::vector<std::string> ForestStateFuture::getOptions() {
    return std::vector<std::string>{"Prozkoumej les", "Pozdrav mimozemstana", "Omrkni vesmirnou lod",
                                    "Mrkni na pracku"};
}