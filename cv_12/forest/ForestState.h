//
// Created by xjakubek on 02.12.2021.
//

#ifndef CV_12_FORESTSTATE_H
#define CV_12_FORESTSTATE_H

#include <iostream>
#include <vector>
#include "../Item.h"
#include "../NPC.h"

class ForestState {
public:
    virtual std::string getDescription() = 0;

    virtual std::vector<std::string> getOptions() = 0;

    virtual std::vector<Item *> getItems() = 0;

    virtual std::vector<NPC *> getNPCs() = 0;
};


#endif //CV_12_FORESTSTATE_H
