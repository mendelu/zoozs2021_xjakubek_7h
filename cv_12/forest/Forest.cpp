//
// Created by xjakubek on 02.12.2021.
//

#include "Forest.h"

Forest::Forest() {
    m_description = "";
    m_items = {};
    m_options = {};
    m_npcs = {};
    m_time = EnumTime::Present;
    m_state = nullptr;

    changeState(m_time);
}

void Forest::changeState(EnumTime time) {
    std::cout << std::endl << "----- CHANGE STAGE --- " << std::endl;

    m_time = time;

    if (m_state != nullptr) {
        delete m_state;
    }

    switch (m_time) {
        case EnumTime::Past:
            m_state = new ForestStatePast();
            break;
        case EnumTime::Present:
            m_state = new ForestStatePresent();
            break;
        case EnumTime::Future:
            m_state = new ForestStateFuture();
            break;
        default:
            std::cerr << "Neznamy stav!" << std::endl;
            m_state = new ForestStatePresent();
            break;
    }
    makeChanges();
}

void Forest::makeChanges() {
    setDescription(m_state->getDescription());
    setItems(m_state->getItems());
    setNPCs(m_state->getNPCs());
    setOptions(m_state->getOptions());
}

void Forest::setDescription(std::string desc) {
    m_description = desc;
}

void Forest::setNPCs(std::vector<NPC *> n) {
    m_npcs = n;
}

void Forest::setItems(std::vector<Item *> i) {
    m_items = i;
}

void Forest::setOptions(std::vector<std::string> o) {
    m_options = o;
}

void Forest::printInfo() {
    std::cout << "Les: " << m_description << std::endl;

    std::cout << "Options: " << std::endl;
    for (int i = 0; i < m_options.size(); ++i) {
        std::cout << "[" << i << "] - " << m_options.at(i) << std::endl;
    }

    std::cout << "Items: " << std::endl;
    for (int i = 0; i < m_items.size(); ++i) {
        std::cout << "[" << i << "] - ";
        m_items.at(i)->printInfo();
    }

    std::cout << "NPCs: " << std::endl;
    for (int i = 0; i < m_npcs.size(); ++i) {
        std::cout << "[" << i << "] - ";
        m_npcs.at(i)->printInfo();
    }
}