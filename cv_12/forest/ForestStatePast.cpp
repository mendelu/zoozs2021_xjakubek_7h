//
// Created by xjakubek on 02.12.2021.
//

#include "ForestStatePast.h"

std::string ForestStatePast::getDescription() {
    return "Nachazis se v lese v minulosti";
}

std::vector<Item *> ForestStatePast::getItems() {
    std::vector<Item *> items = {new Item("Historicka kosa", "S timhle se kosila trava")};
    return items;
}

std::vector<std::string> ForestStatePast::getOptions() {
    return std::vector<std::string>{"Prozkumej les", "Pozdrav rolnika", "Seber minci"};
}

std::vector<NPC *> ForestStatePast::getNPCs() {
    return std::vector<NPC *>{new NPC("Rolnik", "Pozdrav panbuh")};
}