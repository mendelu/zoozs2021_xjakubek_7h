#include <iostream>
#include "forest/Forest.h"

int main() {
    Forest* forest = new Forest();

    forest->printInfo();

    forest->changeState(EnumTime::Future);
    forest->printInfo();

    delete forest;
    return 0;
}
