//
// Created by xjakubek on 02.12.2021.
//

#ifndef CV_12_NPC_H
#define CV_12_NPC_H

#include <iostream>

class NPC {
    std::string m_name;
    std::string m_greeting;

public:
    NPC(std::string name, std::string greeting);

    void printInfo();
};


#endif //CV_12_NPC_H
