//
// Created by xjakubek on 02.12.2021.
//

#ifndef CV_12_ITEM_H
#define CV_12_ITEM_H

#include <iostream>

class Item {
    std::string m_description;
    std::string m_name;

public:

    Item(std::string name, std::string description);

    void printInfo();
};


#endif //CV_12_ITEM_H
