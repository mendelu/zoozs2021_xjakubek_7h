//
// Created by xjakubek on 02.12.2021.
//

#include "NPC.h"

NPC::NPC(std::string name, std::string greeting) {
    m_name = name;
    m_greeting = greeting;
}


void NPC::printInfo() {
    std::cout << "NPC " << m_name << ": " << m_greeting << std::endl;
}