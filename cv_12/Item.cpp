//
// Created by xjakubek on 02.12.2021.
//

#include "Item.h"

Item::Item(std::string name, std::string description) {
    m_name = name;
    m_description = description;
}

void Item::printInfo() {
    std::cout << "Item " << m_name << ": " << m_description << std::endl;
}