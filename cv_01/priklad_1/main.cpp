#include <iostream>

using namespace std;

class Sportovec {
public:
    string m_meno;
    float m_celkomKm;

    void setMeno(string noveMeno) {
        m_meno = noveMeno;
    }

    void behaj(float noveKm) {
        m_celkomKm = m_celkomKm + noveKm;
//        m_celkomKm += noveKm;
    }

    float getCelkomKm() {
        return m_celkomKm;
    }

    void printInfo() {
        std::cout << "Meno: " << m_meno << std::endl;
        std::cout << "Celkom km: " << getCelkomKm() << std::endl;
    }

};

int main() {
    std::cout << "Hello Tomas" << std::endl;
    return 0;
}
