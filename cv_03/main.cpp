#include <iostream>

using namespace std;

class Programator {
private:
    string m_meno;
    int m_hodinovaMzda;
    int m_pocetHodin;
    int m_bonusZaNadcas;

public:
    Programator(string meno, int mzda, int bonus) {
        m_meno = meno;
        m_hodinovaMzda = mzda;
        m_bonusZaNadcas = bonus;
        m_pocetHodin = 0;
    }

    Programator(string meno) {
        m_meno = meno;
        m_hodinovaMzda = 300;
        m_bonusZaNadcas = 50;
        m_pocetHodin = 0;
    }

    void pracuj(int pocetOdpracovanychHodin) {
        m_pocetHodin += pocetOdpracovanychHodin;
//        m_pocetHodin = m_pocetHodin + pocetOdpracovanychHodin;
    }

    void koniecMesiaca() {
        m_pocetHodin = 0;
    }

    int getMzda() {
        int mzda = m_pocetHodin * m_hodinovaMzda;
        if (m_pocetHodin >= 40) {
            mzda = mzda + (m_pocetHodin - 40) * m_bonusZaNadcas;
        }
        return mzda;
    }

    void printInfo() {
        cout << "Meno: " << m_meno << endl;
        cout << "Mzda: " << getMzda() << endl;
    }

    ~Programator() {
        cout << "Programator skoncil!" << endl;
    }
};

int main() {
    Programator *tom = new Programator("Tomas", 400, 100);
    tom->printInfo();
    tom->pracuj(100);
    tom->printInfo();
    tom->koniecMesiaca();
    tom->printInfo();

    delete tom;
    return 0;
}
